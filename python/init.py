from pathlib import Path

import dask.dataframe as dd
import pandas as pd
import data_handler

from dask.diagnostics import ProgressBar

import os

path_data_folder = Path(r"H:\work\freelance\nastenka")
filename_sharenames_to_process_txt = "ric_names_shares.txt"


def batch_processor(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, columns_of_interest="all", filename_addendum=""):

    if filename_addendum == "":
        addendum = ""
    else:
        addendum = "_" + filename_addendum

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt)
    ric_names_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'], dtype='string')
    ric_names_df = ric_names_df.RIC

    for line in ric_names_df:
        print("processing quotes for", line)
        ric_name = line[:-3]
        filename_output = ric_name + addendum + ".raw.csv"
        print("")
        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = data_handler.filter_single_share(df, ric_name)

        #df = df[df.Volume > 0]

        _browsable_aray = df.head(100)

        with ProgressBar():
            df.to_csv(path_data_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        # break
    print("done")


def batch_processor_old(path_data_folder, filename_sharenames_to_process_txt, filename_datafile_csv):
    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt)
    ric_names_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'], dtype='string')
    ric_names_df = ric_names_df.RIC

    for line in ric_names_df:
        print("processing quotes for", line)
        ric_name = line[:-3]
        filename_output = ric_name + "_volumes.raw.csv"

        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        print(df.head())

        df = data_handler.filter_single_ric_and_volume(df, "Volume", ric_name)

        wtf = df.head(100, compute=True)

        with ProgressBar():
            df.to_csv(path_data_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        # break
    print("done")


def post_processing(path_metadata_folder):

    try:
        os.mkdir(path_metadata_folder.joinpath("processed"))
        print("creating folder to save processed data")

    except:
        print("folder for processed data already exists")

    for file in path_metadata_folder.glob('*.csv'):

        data_handler.post_processing(path_metadata_folder, file.name, str(path_data_folder).split("_")[0], True)


def construct_ba_spread(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, path_output_folder):

    columns_of_interest = ["RIC", "Datetime", "Bidprice", "Askprice"]

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt)
    ric_names_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'],
                               dtype='string')
    ric_names_df = ric_names_df.RIC

    for line in ric_names_df:
        print("processing quotes for", line)
        ric_name = line[:-3]
        filename_output = "BAspread_" + ric_name + ".csv"

        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = data_handler.filter_single_share(df, ric_name)

        df = data_handler.column_filter(df, columns_of_interest)

        df = data_handler.dropna_numeric(df)

        df = data_handler.fix_datetime(df)

        df = data_handler.construct_ba_spread(df)


        with ProgressBar():
            df.to_csv(path_output_folder.joinpath(filename_output)
                      , index=False, single_file=True, header_first_partition_only=True)


    print("done")


def june_task_1(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt):
    batch_processor(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, filename_addendum="only_quotes")


def june_task_2_preprocessing(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, propriety_of_interest):

    if propriety_of_interest == "":
        addendum = ""
    else:
        addendum = "_" + propriety_of_interest

    path_output_folder = path_data_folder.joinpath(propriety_of_interest + "_Metadata")

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt, ":")
    ric_identifier_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'], dtype='string')
    ric_identifier_df = ric_identifier_df.RIC

    # more print outputs
    for line in ric_identifier_df:
        print(line, end=",")

    print("\n")

    for line in ric_identifier_df:
        print("processing quotes for", line)
        print("")

        company_name = line[:-3]  # used for regex motif

        if company_name == "this wont matter, replace to skip call":
            print("skip call")

        else:
            print("processing call:")
            df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

            df = data_handler.filter_call(df, company_name)

            df = data_handler.filter_out_nans_in_ask_bid(df)

            df = data_handler.construct_depth(df)

            df = df.drop(["Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

            _browsable_aray = df.head(100)

            filename_output = company_name + addendum + ".call.raw.csv"
            print("writing to:", path_output_folder.joinpath(filename_output))
            with ProgressBar():
                df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        print("processing put:")
        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = data_handler.filter_put(df, company_name)

        df = data_handler.filter_out_nans_in_ask_bid(df)

        df = data_handler.construct_depth(df)

        df = df.drop(["Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"],
                     axis=1)

        #_browsable_aray = df.head(100)

        filename_output = company_name + addendum + ".put.raw.csv"
        print("writing to:", path_output_folder.joinpath(filename_output))
        with ProgressBar():
            df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        # break
    print("done")


def preprocess_volumes(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, filename_addendum, name_metadata_outputfolder):

    if filename_addendum == "":
        addendum = ""
    else:
        addendum = "_" + filename_addendum

    path_output_folder = path_data_folder.joinpath(name_metadata_outputfolder)

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt, ":")
    ric_identifier_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'], dtype='string')
    ric_identifier_df = ric_identifier_df.RIC

    # more print outputs
    for line in ric_identifier_df:
        print(line, end=",")

    print("\n")

    for line in ric_identifier_df:
        print("processing quotes for", line)
        print("")

        company_name = line[:-3]  # used for regex motif

        if company_name == "this wont matter, replace to skip call":
            print("skip call")

        else:
            print("processing call:")
            df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

            df = data_handler.filter_call(df, company_name)

            df = df[df.Volume > 0]

            df = df.drop(["Domain", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

            # _browsable_aray = df.head(100)

            filename_output = company_name + addendum + ".call.raw.csv"
            print("writing to:", path_output_folder.joinpath(filename_output))
            with ProgressBar():
                df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        print("processing put:")
        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = data_handler.filter_put(df, company_name)

        df = df[df.Volume > 0]

        df = df.drop(["Domain", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

        #_browsable_aray = df.head(100)

        filename_output = company_name + addendum + ".put.raw.csv"
        print("writing to:", path_output_folder.joinpath(filename_output))
        with ProgressBar():
            df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True, header_first_partition_only=True)

        # break
    print("done")


def construct_excel(path_output_folder, filename_excel_template, filename_ric_names_shares, str_propriety_of_interest, callorput):

    # get timeseries for excel
    filepath_excel_template = path_output_folder.joinpath(filename_excel_template)
    timeseries = data_handler.construct_excel_timeseries(filepath_excel_template)
    timeseries = timeseries.apply(lambda time: str(time).replace("-", '/')[2:-3])
    timeseries = timeseries.apply(lambda time: time[6:8] + "/" + time[3:5] + "/" + time[:2] + " " + time[9:])

    # get column names for excel
    series_ric_identifier = pd.read_csv(path_output_folder.joinpath(filename_ric_names_shares), names=['RIC'], dtype='string')
    series_ric_identifier = series_ric_identifier["RIC"]
    list_company_names = list(series_ric_identifier.apply(lambda ric: ric[:-3]))
    list_columns = ["datum_zeit"] + list_company_names


    # construct excel
    df_output = pd.DataFrame(columns=list_columns)
    df_output.datum_zeit = timeseries

    path_data_folder = path_output_folder.joinpath(str_propriety_of_interest + "_Metadata").joinpath("processed")
    for file in path_data_folder.glob('*.' + callorput + '.csv'):
        print("getting data from ", file)
        str_company_name = file.name.split("_")[0]
        df_data = pd.read_csv(path_data_folder.joinpath(file.name))
        series_timestep_data = df_data["Datetime"]
        series_propriety_of_interest_data = df_data[str_propriety_of_interest]

        series_propriety_of_interest_output = pd.Series([])

        """if str_company_name == "ACE":
            break"""  # this is handy for debug, just unindent the next paragraph

        # construct series with data of interest to replace column in output df
        for timestep in df_output.datum_zeit:
            series_data_matching_bool = series_timestep_data.str.find(timestep)
            try:
                index_data = series_data_matching_bool[series_data_matching_bool >= 0].index[0]
                value_of_interest = series_propriety_of_interest_data.iloc[index_data]
                series_propriety_of_interest_output = series_propriety_of_interest_output.append(pd.Series([value_of_interest]))

            except IndexError:
                series_propriety_of_interest_output = series_propriety_of_interest_output.append(pd.Series([0]))

        print("apply", str_company_name, "data to output df")
        df_output[str_company_name] = series_propriety_of_interest_output.values

    filename_output = str_propriety_of_interest + "." + callorput + ".xlsx"
    print("writing data to", filename_output)
    df_output.to_excel(path_output_folder.joinpath(filename_output))

    print("done")


def preprocess_espr(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, propriety_of_interest):

    if propriety_of_interest == "":
        addendum = ""
    else:
        addendum = "_" + propriety_of_interest

    path_output_folder = path_data_folder.joinpath(propriety_of_interest + "_Metadata")

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt, ":")
    ric_identifier_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'],
                                    dtype='string')
    ric_identifier_df = ric_identifier_df.RIC

    # more print outputs
    for line in ric_identifier_df:
        print(line, end=",")

    print("\n")

    for line in ric_identifier_df:
        print("processing quotes for", line)
        print("")

        company_name = line[:-3]  # used for regex motif

        if company_name == "insertcompanyname":
            print("skip call")

        else:
            print("processing call:")
            df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

            df = data_handler.filter_out_nans_in_ask_bid(df)

            df = data_handler.filter_call(df, company_name)

            _browsable_aray = df.head(100)

            df = data_handler.construct_pmid(df)

            df = df.drop(["Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

            _browsable_aray = df.head(100)

            filename_output = company_name + addendum + ".call.raw.csv"
            print("writing to:", path_output_folder.joinpath(filename_output))
            with ProgressBar():
                df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True,
                          header_first_partition_only=True)

        print("processing put:")
        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = data_handler.filter_put(df, company_name)

        df = data_handler.filter_out_nans_in_ask_bid(df)

        df = data_handler.construct_pmid(df)

        df = df.drop(["Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"],
                     axis=1)

        # _browsable_aray = df.head(100)

        filename_output = company_name + addendum + ".put.raw.csv"
        print("writing to:", path_output_folder.joinpath(filename_output))
        with ProgressBar():
            df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True,
                      header_first_partition_only=True)

        # break
    print("done")


def preprocess_PT(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, propriety_of_interest):

    if propriety_of_interest == "":
        addendum = ""
    else:
        addendum = "_" + propriety_of_interest

    path_output_folder = path_data_folder.joinpath(propriety_of_interest + "_Metadata")

    print("initializing batch processing of share names present in", filename_sharenames_to_process_txt, ":")
    ric_identifier_df = dd.read_csv(path_data_folder.joinpath(filename_sharenames_to_process_txt), names=['RIC'],
                                    dtype='string')
    ric_identifier_df = ric_identifier_df.RIC

    # more print outputs
    for line in ric_identifier_df:
        print(line, end=",")

    print("\n")

    for line in ric_identifier_df:
        print("processing quotes for", line)
        print("")

        company_name = line[:-3]  # used for regex motif

        if company_name == "GASI":
            print("skip call")

        else:
            print("processing call:")
            df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

            _browsable_aray = df.head(100)

            df = df.drop(["Domain", "Volume", "Type", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

            df = data_handler.filter_call(df, company_name)

            _browsable_aray = df.head(100)

            df = df[df.Price > 0]

            _browsable_aray = df.head(100)

            filename_output = company_name + addendum + ".call.raw.csv"
            print("writing to:", path_output_folder.joinpath(filename_output))
            with ProgressBar():
                df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True,
                          header_first_partition_only=True)

        print("processing put:")
        df = data_handler.construct_data_df(path_data_folder, filename_datafile_csv)

        df = df.drop(["Domain", "Volume", "Type", "Bidprice", "Bidsize", "Askprice", "Asksize"], axis=1)

        df = data_handler.filter_put(df, company_name)

        df = df[df.Price > 0]

        # _browsable_aray = df.head(100)

        filename_output = company_name + addendum + ".put.raw.csv"
        print("writing to:", path_output_folder.joinpath(filename_output))
        with ProgressBar():
            df.to_csv(path_output_folder.joinpath(filename_output), index=False, single_file=True,
                      header_first_partition_only=True)

        # break
    print("done")
