from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

import init

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def myround(x, base=5):
    return base * round(x/base)


path_data_folder = Path(r"G:\gp66 ddisk 202209\nastenka")
filename_datafile_csv = "A2_volumes.csv"

pdf_input = pd.read_csv(path_data_folder.joinpath(filename_datafile_csv))
pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: time[:16].replace("T", '').replace("-", '').replace(":", ''))
pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: myround(int(time)))
pdf_input = pdf_input.sort_values("Datetime")
pdf_input = pdf_input.drop(axis=1, labels='RIC')

pdf_output = pd.DataFrame()
timestamp = pdf_input.iloc[0][0]
volume = 0
for row in pdf_input.iterrows():
    if row[1][0] == timestamp:
        volume += row[1][1]
    else:
        # print(timestamp, volume)
        pdf_output = pdf_output.append({"Datetime": timestamp, "Volume": volume}, ignore_index=True)
        timestamp = row[1][0]
        volume = row[1][1]
pdf_output = pdf_output.append({"Datetime": timestamp, "Volume": volume}, ignore_index=True)  # this adds the last line which would be skipped otherwise

# write
pdf_output.to_csv(path_data_folder.joinpath("test2.csv"), index=False)
