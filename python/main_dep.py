import pathlib
from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

import init

path_data_folder = Path(r"H:\work\freelance\nastenka")
filename_datafile_csv = "pt_eurex_options_spain_part_1b_4.csv"
filename_sharenames_to_process_txt = "todo.txt"


init.june_task_2_preprocessing(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, "DEP")

