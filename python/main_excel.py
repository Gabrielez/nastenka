import pathlib
from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

import init


path_data_folder = Path(r"H:\work\freelance\nastenka")
path_output_folder = path_data_folder.joinpath("BAspread")
filename_datafile_csv = "pt_eurex_options_spain_part_1b_4.csv"
filename_sharenames_to_process_txt = "todo.txt"
"""
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "QSPR", "call")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "QSPR", "put")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "PQSPR", "call")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "PQSPR", "put")"""
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "DEP", "call")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "DEP", "put")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "Volume", "call")
init.construct_excel(path_data_folder, "vorlage_excel.xlsx", "ric_names_shares.txt", "Volume", "put")