import pathlib
from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

import init

path_data_folder = Path(r"H:\work\freelance\nastenka")
path_output_folder = path_data_folder.joinpath("BAspread")
filename_datafile_csv = "pt_eurex_options_spain_part_1b_4.csv"
filename_sharenames_to_process_txt = "todo.txt"


init.preprocess_volumes(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, "Volume", "Volume_Metadata")

# init.june_task_2_preprocessing(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, "DEP", "DEP_Metadata")

# init.batch_processor_old(path_data_folder, filename_sharenames_to_process_txt, filename_datafile_csv)

# init.post_processing(path_output_folder)

# init.construct_ba_spread(path_data_folder, filename_datafile_csv, filename_sharenames_to_process_txt, path_output_folder)
