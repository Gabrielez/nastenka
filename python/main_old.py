from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

path_data_folder = Path(r"G:\gp66 ddisk 202209\nastenka")
filename = "pt_eurex_options_spain_part_1b_4.csv"

print(path_data_folder)

dict_dtypes = {"#RIC": 'string', "Date-Time": 'string', "Volume": 'Int64'}
dict_column_rename = {"#RIC": "RIC", "Date-Time": "Datetime", "Bid Price": "Bidprice", "Bid Size": "Bidsize", "Ask Price": "Askprice", "Ask Size": "Asksize"}
# header = ["RIC","Domain","Datetime","Type","Price","Volume","Bidprice","Bidsize","Askprice","Asksize"]
# df = dd.read_csv(path_data_folder.joinpath("quotes/A2*.MI.txt"), names=header, assume_missing=True, dtype=dict_dtypes)
df = dd.read_csv(path_data_folder.joinpath(filename), assume_missing=True, dtype=dict_dtypes)
print(df.head())
df = df.rename(columns=dict_column_rename)
df = df.drop(["Domain", "Type","Price","Bidprice","Bidsize", "Askprice", "Asksize"], axis=1)

print(df.head())

rics = df.RIC
matched = rics.str.match("A2[0-9]")

print(matched.head())

onlymatching = df.loc[matched]
#onlymatching = onlymatching.repartition(npartitions=500)

volumes = onlymatching.Volume

volumes_relevant = volumes[volumes > 0]
print(volumes_relevant.head())
print(df.loc[639912].head())
print(df.loc[713290].head())
print(df.loc[757707].head())

#print(volumes_relevant.tail())

# with ProgressBar():
    # print(volumes_relevant.compute())

"""
def datetime_converter(string):
    return string.replace("-","").replace(":","").replace("T","")[:12]


times = df.Datetime.apply(datetime_converter, meta=('Datetime', np.int32)).to_frame()
#print(times)

volumes = df.Volume.to_frame()
print(volumes)

volumes_relevant = volumes[volumes.Volume > 0].compute()
print(volumes_relevant)


for index in volumes_relevant.index:
    print(index, " :: ", end="")
    print(times.loc[index].compute())

reduced_df = times.join(volumes)
#print(reduced_df)
"""
"""reduced_df = reduced_df.reset_index()
for row in reduced_df.iterrows():
    print(row)"""
