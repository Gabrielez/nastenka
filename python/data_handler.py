import math
from pathlib import Path
import glob

import numpy as np
import pandas as pd

import dask.dataframe as dd
import dask.array as da
import dask.bag as db

from dask.diagnostics import ProgressBar

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


def myround(x, base=5):
    return base * math.floor(x/base)


def construct_data_df(path_data_folder, filename_data_csv,
                      dict_dtypes={"#RIC": 'string', "Date-Time": 'string', "Volume": 'Int64', "Bid Price": 'float64', "Ask Price": 'float64'},
                      dict_column_rename={"#RIC": "RIC", "Date-Time": "Datetime", "Bid Price": "Bidprice",
                                  "Bid Size": "Bidsize", "Ask Price": "Askprice", "Ask Size": "Asksize"}):
    df = dd.read_csv(path_data_folder.joinpath(filename_data_csv), assume_missing=True, dtype=dict_dtypes)
    df = df.rename(columns=dict_column_rename)
    return df


def filter_single_ric_and_volume(df, column_of_interest, share_of_interest, quotesonly=True):

    list_data_columns_to_remove = ["RIC", "Datetime", "Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"]
    list_data_columns_to_remove.remove(column_of_interest)
    ric_names = df.RIC
    test = ric_names.head(100)
    df = df.drop(list_data_columns_to_remove, axis=1)


    if quotesonly:
        grep_motif = share_of_interest+"[0-9]"  # this grep motives is for share quotes only
    else:
        grep_motif = share_of_interest+"*"  # this is for including the shares themselves

    bool_list_matched = ric_names.str.match(grep_motif)

    df_onlymatching = df.loc[bool_list_matched]

    df_onlymatching = df_onlymatching[df_onlymatching.Volume > 0]

    return df_onlymatching


def filter_out_nans_in_ask_bid(df):

    df = df[df.Askprice > 0]
    df = df[df.Asksize > 0]
    df = df[df.Bidprice > 0]
    df = df[df.Bidsize > 0]

    return df


def filter_call(df, ric_of_interest):

    grep_motif = ric_of_interest + "\d+[A-L]"
    print("grep motif used for call filtering: ", grep_motif)

    ric_names = df.RIC
    bool_list_matched = ric_names.str.match(grep_motif)

    df_onlymatching = df.loc[bool_list_matched]

    return df_onlymatching


def filter_put(df, ric_of_interest):

    grep_motif = ric_of_interest + "\d+[M-X]"
    print("grep motif used for put filtering: ", grep_motif)

    ric_names = df.RIC
    bool_list_matched = ric_names.str.match(grep_motif)

    df_onlymatching = df.loc[bool_list_matched]

    return df_onlymatching


def filter_single_share(df, share_of_interest, quotesonly=True):

    ric_names = df.RIC

    if quotesonly:
        grep_motif = share_of_interest + "[0-9]"  # this grep motives is for share quotes only
    else:
        grep_motif = share_of_interest+"*"  # this is for including the shares themselves

    bool_list_matched = ric_names.str.match(grep_motif)
    df_onlymatching = df.loc[bool_list_matched]

    return df_onlymatching


def post_processing_old(path_metadata_folder, filename):
    path_output_folder = path_metadata_folder.joinpath("processed")
    
    pdf_input = pd.read_csv(path_metadata_folder.joinpath(filename))
    pdf_input.Datetime = pdf_input.Datetime.apply(
        lambda time: time[:16].replace("T", '').replace("-", '').replace(":", ''))
    pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: myround(int(time)))
    pdf_input = pdf_input.sort_values("Datetime")
    pdf_input = pdf_input.drop(axis=1, labels='RIC')

    pdf_output = pd.DataFrame()
    timestamp = pdf_input.iloc[0][0]
    volume = 0
    for row in pdf_input.iterrows():
        if row[1][0] == timestamp:
            volume += row[1][1]
        else:
            # print(timestamp, volume)
            pdf_output = pdf_output.append({"Datetime": timestamp, "Volume": volume}, ignore_index=True)
            timestamp = row[1][0]
            volume = row[1][1]
    pdf_output = pdf_output.append({"Datetime": timestamp, "Volume": volume},
                                   ignore_index=True)  # this adds the last line which would be skipped otherwise

    # write
    pdf_output.to_csv(path_output_folder.joinpath(filename), index=False)


def post_processing(path_metadata_folder, filename_input, column_of_interest, calculate_mean=True):

    print("begin post processing for: ", path_metadata_folder.joinpath(filename_input))
    path_output_folder = path_metadata_folder.joinpath("processed")
    filename_output = filename_input[:-7]+"csv"

    pdf_input = pd.read_csv(path_metadata_folder.joinpath(filename_input))
    pdf_input = pdf_input.drop(axis=1, labels='RIC')


    pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: time[:16].replace("T", '').replace("-", '').replace(":", ''))
    pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: myround(int(time)))
    pdf_input = pdf_input.sort_values("Datetime")
    pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: str(time))  # saving as int doesn't work, so string it is
    pdf_input.Datetime = pdf_input.Datetime.apply(lambda time: time[6:8] + "/" + time[4:6] + "/" + time[2:4] + " " + time[8:10] + ":" + time[10:])

    pdf_output = pd.DataFrame({'Datetime': [], column_of_interest: []})

    timestamp = pdf_input.iloc[0][0]
    cummulative_propriety_of_interest = 0
    n_proprieties_added = 0
    propriety_of_interest = 0
    for row in pdf_input.iterrows():
        if row[1][0] == timestamp:
            cummulative_propriety_of_interest += row[1][1]
            n_proprieties_added += 1
        else:
            # write to output
            if calculate_mean:
                propriety_of_interest = cummulative_propriety_of_interest / n_proprieties_added
            else:
                propriety_of_interest = str(cummulative_propriety_of_interest)
            pdf_output = pdf_output.append({"Datetime": timestamp, column_of_interest: propriety_of_interest}, ignore_index=True)

            # reset and proceed to next timestep
            timestamp = row[1][0]
            cummulative_propriety_of_interest = row[1][1]
            n_proprieties_added = 1
    pdf_output = pdf_output.append({"Datetime": timestamp, column_of_interest: propriety_of_interest}, ignore_index=True)  # this adds the last line which would be skipped otherwise

    # write
    print("writing to: ", path_output_folder.joinpath(filename_output))
    pdf_output.to_csv(path_output_folder.joinpath(filename_output), index=False)


def column_filter(df, list_columns_of_interest):

    list_data_columns = ["RIC", "Datetime", "Domain", "Volume", "Type", "Price", "Bidprice", "Bidsize", "Askprice", "Asksize"]
    for column in list_columns_of_interest:
        list_data_columns.remove(column)

    df = df.drop(list_data_columns, axis=1)

    return df


def dropna_numeric(df):

    index = 0
    for dtype in df.dtypes:
        if dtype == "float64" or dtype == "Int64":
            column_name = (df.columns[index])
            df = df[df[column_name] > 0]
        index += 1

    return df


def construct_ba_spread(df):

    return df.assign(BAspread=lambda x: ((x.Askprice - x.Bidprice) / x.Askprice))


def construct_quoted_spread(df):

    return df.assign(QSPR=lambda x: (x.Askprice - x.Bidprice))


def construct_proportional_quoted_spread(df):

    return df.assign(PQSPR=lambda x: ((x.Askprice - x.Bidprice) / ((x.Askprice + x.Bidprice) / 2)))


def construct_depth(df):

    return df.assign(DEP=lambda x: ((x.Asksize + x.Bidsize) / 2))


def construct_pmid(df):

    return df.assign(PMID=lambda x: ((x.Askprice + x.Bidprice) / 2))


def fix_datetime(df_with_datetime):

    df_with_datetime.Datetime = df_with_datetime.apply(
        lambda df: df.Datetime[:16].replace("T", '').replace("-", '').replace(":", ''),
        axis=1, meta=df_with_datetime.Datetime)

    return df_with_datetime


def construct_excel_timeseries(filename_excel_template):

    df = pd.read_excel(filename_excel_template)

    df = df[df.columns[0]]  # first column only
    df = df.dropna()

    return df